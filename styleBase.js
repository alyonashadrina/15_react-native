import { StyleSheet } from 'react-native';

let colorsBase = {
  blue: '#00b3be',
  white: '#fff',
  grey: '#4a4a4a',
  green: '#91d763',
  red: 'red',
  lightgrey: 'lightgrey'
};

export const spaces = {
    xsm: 8,
    sm: 16,
    md: 24,
}

export const colors = {
  primary: colorsBase.blue,
  onPrimaryBg: colorsBase.white,
  text: colorsBase.grey,
  selected: colorsBase.green,
  error: colorsBase.red,
  border: colorsBase.lightgrey,
}

export const typography = StyleSheet.create({
  h6: {
    fontSize: 20,
    letterSpacing: .15,
    fontWeight: 'bold'
  },
  body1: {
    fontSize: 16,
    letterSpacing: .5
  },
  subtitle1: {
    fontSize: 16,
    letterSpacing: .15
  },
  button: {
    textTransform: 'uppercase',
    fontSize: 14,
    letterSpacing: 1.25
  }
});
