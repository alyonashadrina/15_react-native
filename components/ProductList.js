import React from 'react';
import { StyleSheet, Text, View, Alert, Dimensions, TouchableOpacity, Image, FlatList, ScrollView, Button, TextInput, ImageBackground } from 'react-native';

import { typography, colors, spaces } from '../styleBase';

const rows = 3;
const itemHeight = (Dimensions.get('window').height / rows) - (spaces.sm * (rows + 1));


const ListItem = ({ product, key }) => {
  const img = ( product.images && product.images[0] ? product.images[0].file : 'http://www.liberaldictionary.com/wp-content/uploads/2018/12/item.png' )
  const text = ( product.text ? product.text : 'Item' )

  return (
    <View key={key} style={styles.listItem}>
      <Image
        style={styles.listItemImg}
        source={{uri: img}}
      />
      <Text style={[typography.subtitle1, styles.listItemText]}>{text}</Text>
    </View>
  )
}

class ProductList extends React.Component {
  constructor() {
    super();
    this.state = {
      ProductList: [],
      searchWord: ''
    },
    this.getFilterderProducts = this.getFilterderProducts.bind(this);
    this.clearSearchWord = this.clearSearchWord.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Products',
      headerRight: (
        <View>
          <TouchableOpacity
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
            onPress={() => navigation.navigate('Profile')}
            >
            <Text style={styles.listItemText}>btn</Text>
          </TouchableOpacity>
        </View>
      ),
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTitleStyle: {
        color: colors.onPrimaryBg
      },
    }
  }

  componentDidMount() {
    fetch('http://light-it-04.tk/api/posters/?format=json')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({ProductList: responseJson.data})
    })
    .catch((error) => {
      console.error(error);
    });
  }

  getFilterderProducts() {
    fetch(`http://light-it-04.tk/api/posters/?search=${this.state.searchWord}`)
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({ProductList: responseJson.data})
    })
    .catch((error) => {
      console.error(error);
    });
  }

  clearSearchWord() {
    fetch('http://light-it-04.tk/api/posters/?format=json')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({ProductList: responseJson.data, searchWord: ''})
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {
    return (
      <ScrollView >
        <View style={{position: 'relative'}}>
          <TextInput placeholder="Search" onBlur={this.getFilterderProducts} value={this.state.searchWord} onChange={(e) => this.setState({searchWord: e.nativeEvent.text})} style={styles.search}/>
          <TouchableOpacity hitSlop={{top: 20, bottom: 20, left: 50, right: 50}} onPress={this.clearSearchWord} style={{flex: 1, justifyContent: 'center', alignItems: 'center', width: 60, height: 60, position: 'absolute', right: 16, top: 16}}>
            <Image source={require('../assets/icon-outline_clear_black_24dp.png')} />
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.ProductList}
          style={styles.productList}
          renderItem={({item, key}) => <ListItem key={key} product={item}/>}
        />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  productList: {
    flex: 1,
  },
  listItem: {
    padding: spaces.sm,
    width: Dimensions.get('window').width,
    height: itemHeight + spaces.sm,
    marginBottom: spaces.sm
  },
  listItemImg: {
    height: itemHeight - spaces.sm * 2,
  },
  listItemText: {
    backgroundColor: colors.primary,
    padding: spaces.xsm,
    color: colors.onPrimaryBg,
  },
  search: {
    padding: spaces.sm,
    borderColor: colors.border,
    borderRadius: 5,
    borderWidth: 1,
    width: Dimensions.get('window').width - spaces.sm * 2 ,
    marginLeft: spaces.sm,
    marginRight: spaces.sm,
    marginTop: spaces.sm,
  }
});

export default ProductList
