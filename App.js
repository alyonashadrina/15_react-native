import React from "react";
import { View, Text, FlatList, Image } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import ProductList from './components/ProductList'
import Profile from './components/Profile'

const AppNavigator = createStackNavigator({
  Home: {
    screen: ProductList
  },
  Profile: {
    screen: Profile
  }
});

export default createAppContainer(AppNavigator);
